package com.bancaordine.controller;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.service.ItemServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;


@RestController
@RequestMapping(value = "/api/item")
public class ItemControllerImpl implements ItemControllerInterface {
    
    @Autowired
    private ItemServiceInterface itemService;


    @PostMapping(value = "/store")
    @Transactional(TxType.REQUIRED)
    public ResponseEntity<ItemDto> store( @RequestBody ItemDto itemDtoRequest){

        ItemDto itemDto = itemService.store(itemDtoRequest);

        return new ResponseEntity<>(itemDto,HttpStatus.OK);
        
    }


    @DeleteMapping(value = "/delete/{id}")
    @Transactional(TxType.REQUIRED)
    public ResponseEntity<String> delete(@PathVariable(name = "id") int id){

        if(!itemService.delete(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Item not founded");
        }

        return new ResponseEntity<>("Item eliminato",HttpStatus.OK);
    }

}
