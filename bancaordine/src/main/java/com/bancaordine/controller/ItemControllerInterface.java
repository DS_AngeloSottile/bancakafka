package com.bancaordine.controller;

import com.bancaordine.model.dto.ItemDto;

import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

public interface ItemControllerInterface {
    
    @Operation(summary = "Make an update of one item")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return the current item like feedback to the user", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ItemDto.class)) 
                                    }),
                    }) 
    ResponseEntity<ItemDto> store(  ItemDto itemDtoRequest);

    @Operation(summary = "Delete the current item from the database and the list of the users")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return a string with a feedback message", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json") 
                                    }),
                    }) 
    ResponseEntity<String> delete( int id);
}
