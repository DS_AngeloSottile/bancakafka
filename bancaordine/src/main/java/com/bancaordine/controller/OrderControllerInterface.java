package com.bancaordine.controller;

import java.util.List;

import com.bancaordine.model.dto.ErrorDto;
import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.dto.OrderDto;
import com.bancaordine.model.dto.OrderDtoRequest;

import org.springframework.http.ResponseEntity;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;


public interface OrderControllerInterface {
    
    @Operation(summary = "Make an order of any type by insert a list of items")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return the current order already done", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = OrderDto.class)) 
                                    }),
                        @ApiResponse(
                            responseCode = "406", 
                            description = "Exception launched when the item is not present on the database", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ErrorDto.class)) 
                                    })
                    }) 
    ResponseEntity<OrderDto> makeOrder(List<ItemDto> orderDtoRequest);

    @Operation(summary = "Get the order by passing the id ")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return the current order founded by id", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = OrderDto.class)) 
                                    }),
                        @ApiResponse(
                            responseCode = "500", 
                            description = "The argument passed is null", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ErrorDto.class)) 
                                    })
                    }) 
    ResponseEntity<OrderDto> getOrder( OrderDtoRequest orderDtoRequest );

    @Operation(summary = "Return the current status of the order")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return the current order founded by id", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = OrderDto.class)) 
                                    }),
                        @ApiResponse(
                            responseCode = "500", 
                            description = "The argument passed is null", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ErrorDto.class)) 
                                    })
                    })
    ResponseEntity<String> checkOrderPayed(Integer id);

    @Operation(summary = "Make an update to the order you can add new items as well")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return the current order already updated", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = OrderDto.class)) 
                                    }),
                        @ApiResponse(
                            responseCode = "500", 
                            description = "The argument passed is null", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ErrorDto.class)) 
                                    })                                    
                    }) 
    ResponseEntity<OrderDto> update (List<ItemDto> itemsDtoRequest, Integer id); 


    @Operation(summary = "Delete the current order from the database with relative items")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return a string with a feedback message", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json") 
                                    }),
                        @ApiResponse(
                            responseCode = "500", 
                            description = "The argument passed is null", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ErrorDto.class)) 
                                    })
                    }) 
    ResponseEntity<String> delete(Integer id); 
}
