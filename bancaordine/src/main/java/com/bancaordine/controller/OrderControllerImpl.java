package com.bancaordine.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import com.bancaordine.exception.IllegalParameterExcception;
import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.dto.OrderDto;
import com.bancaordine.model.dto.OrderDtoRequest;
import com.bancaordine.model.service.OrderServiceInterface;


@RestController
@RequestMapping(value="/api/order")
public class OrderControllerImpl implements OrderControllerInterface {
    
    private static final String ERROR_MESSAGE = "campo non valido";

    @Autowired
    private OrderServiceInterface orderService;

    @PostMapping(value = "/make")
    public ResponseEntity<OrderDto> makeOrder(@RequestBody List<ItemDto> itemsDtoRequest){

        OrderDto orderDto = orderService.makeOrder(itemsDtoRequest);

        return new ResponseEntity<>(orderDto,HttpStatus.OK);

    }

    @PostMapping(value = "/get")
    public ResponseEntity<OrderDto> getOrder(@RequestBody OrderDtoRequest orderDtoRequest){
        
        OrderDto orderResponseDto = orderService.getOrder(orderDtoRequest);

        return new ResponseEntity<>(orderResponseDto,HttpStatus.OK);
    }
    
    @GetMapping(value = "/checkStatus/{id}")
    public ResponseEntity<String> checkOrderPayed(@PathVariable(name = "id") Integer id){
        
        if( id == null ){
            throw new IllegalParameterExcception(ERROR_MESSAGE,HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        if(orderService.payed(id)){
            return new ResponseEntity<>("Ordine pagato",HttpStatus.OK);

        }

        return new ResponseEntity<>("Ordine ancora da verificare",HttpStatus.OK);
    }
    
    @PostMapping(value = "/update/{id}")
    public ResponseEntity<OrderDto> update( 
        @RequestBody List<ItemDto> itemsDtoRequest,
        @PathVariable(name = "id") Integer id ){

        if( id == null ){
            throw new IllegalParameterExcception(ERROR_MESSAGE,HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        OrderDto orderDto = orderService.update(itemsDtoRequest,id);

        return new ResponseEntity<>(orderDto,HttpStatus.OK);
        
    }
    

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") Integer id){

        if( id == null ){
            throw new IllegalParameterExcception(ERROR_MESSAGE,HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        if(!orderService.delete(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Order not founded");
        }

        return new ResponseEntity<>("Ordine eliminato",HttpStatus.OK);
    } 

}
