package com.bancaordine.controller;

import com.bancaordine.exception.ItemExistException;
import com.bancaordine.exception.IllegalParameterExcception;
import com.bancaordine.model.dto.ErrorDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice(basePackages = "com.bancaordine.controller")
public class ErrorHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

  @ExceptionHandler(IllegalParameterExcception.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorDto handleCustomException(IllegalParameterExcception exception) {
    LOGGER.info("msg");
    return ErrorDto.builder().message(exception.getMessage()).build();
    
  }

  @ExceptionHandler(ItemExistException.class)
  @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  public ErrorDto handleCustomException(ItemExistException exception) {
    LOGGER.info("msg");
    return ErrorDto.builder().message(exception.getMessage()).code(exception.getCode()).build();
    
  }
}
