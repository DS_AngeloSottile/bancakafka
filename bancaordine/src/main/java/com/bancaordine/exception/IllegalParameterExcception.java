package com.bancaordine.exception;

public class IllegalParameterExcception extends RuntimeException {
    private final Integer code;

    public IllegalParameterExcception(String messageString, Integer code) {
		super(messageString);
		this.code = code;
	}

	public Integer getCode() {
	  return this.code;
	}
}
