package com.bancaordine.exception;

public class ItemExistException extends RuntimeException {

    private final Integer code;

    public ItemExistException(String messageString, Integer code) {
		super(messageString);
		this.code = code;
	}

	public Integer getCode() {
	  return this.code;
	}
    
}
