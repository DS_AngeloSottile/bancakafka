package com.bancaordine.model.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.bancaordine.exception.IllegalParameterExcception;
import com.bancaordine.exception.ItemExistException;
import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.dto.OrderDto;
import com.bancaordine.model.dto.OrderDtoRequest;
import com.bancaordine.model.dto.PaymentDto;
import com.bancaordine.model.dto.StatusEnumDto;
import com.bancaordine.model.entityes.Item;
import com.bancaordine.model.entityes.Order;
import com.bancaordine.model.entityes.OrderItems;
import com.bancaordine.model.repositories.ItemRepository;
import com.bancaordine.model.repositories.OrderItemsRepository;
import com.bancaordine.model.repositories.OrderRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class OrderServiceImpl implements OrderServiceInterface{
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private OrderItemsRepository orderItemsRepository;
    
    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private TransfertService transfertService;

    @Autowired
    private MessageService messageService;

    Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    public boolean payed(int id){
        
        Optional<Order> order = orderRepository.findById(id);

        if(order.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Order not founded");
        }


        if(order.get().getStatus().toLowerCase().equals("pagato")){
            return true;
        }
        return false;
    }

    @Transactional(TxType.REQUIRED)
    public OrderDto makeOrder(List<ItemDto> itemsDtoRequest){

        Order order = new Order();
        order.setStatus(StatusEnumDto.VERIFICANDO.toString());
        order.setCode(UUID.randomUUID().toString());
        orderRepository.save(order);


        int amount = 0;

        for (ItemDto itemDto : itemsDtoRequest) {

            Item item = itemRepository.findItemByName(itemDto.getName());

            if(item == null){
                throw new ItemExistException("The item is not present on the database", HttpStatus.NOT_ACCEPTABLE.value());
            }

            OrderItems orderItems = OrderItems  .builder()
                                    .item(item)
                                    .order(order)
                                    .quantity(itemDto.getQuantity())
                                    .build();

            orderItemsRepository.save(orderItems);

            for(int i = 0; i < itemDto.getQuantity(); i++){
                amount += item.getAmount();
            }
        }

        order.setAmount(amount);
        orderRepository.save(order);

        OrderDto orderDto = OrderDto.builder()
                            .code(order.getCode())
                            .status(order.getStatus()) 
                            .amount(order.getAmount())
                            .build();

        messageService.syncSend(orderDto,"testOrder");

        return  orderDto;

    }
    
	public OrderDto getOrder(OrderDtoRequest orderDtoRequest) {
        
        Order order = orderRepository.findOrderByCode(orderDtoRequest.getCode());

        return  transfertService.entity2DtoOrder(order);

	}
                                        //da kafka
    public void paymentCheckInfo(PaymentDto paymentDto){
        Order order = orderRepository.findOrderByCode(paymentDto.getOrderCode());

        switch (paymentDto.getStatus().toLowerCase()) {
            case "pagato":
                order.setStatus(StatusEnumDto.PAGATO.toString());
                break;
        
            case "verifica":
                order.setStatus(StatusEnumDto.VERIFICANDO.toString());
                break;
            case "rifiutato":
                order.setStatus(StatusEnumDto.NON_ACCETTATO.toString());
                break;

            default:
            logger.info("Listener operazioni non consentite");
        }

        orderRepository.save(order);

    }
    
    @Transactional(TxType.REQUIRED)
    public OrderDto update(List<ItemDto> itemsDtoRequest, int id) {

        Optional<Order> order = orderRepository.findById(id);

        if(order.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Order not founded");
        }

        
        order.get().getOrderItems().clear();
        orderRepository.save(order.get());

        int amount = 0;
        for (ItemDto itemDto : itemsDtoRequest) {

            Item item = itemRepository.findItemByName(itemDto.getName());
            itemRepository.save(item);

            OrderItems orderItems = OrderItems  .builder()
                                    .item(item)
                                    .order(order.get())
                                    .quantity(itemDto.getQuantity())
                                    .build();

            orderItemsRepository.save(orderItems);

            for(int i = 0; i < itemDto.getQuantity(); i++){
                amount += item.getAmount();
            }

        }
        order.get().setAmount(amount);
        order.get().setStatus(StatusEnumDto.VERIFICANDO.toString());
        orderRepository.save(order.get());
        

        return OrderDto.builder()
                        .code(order.get().getCode())
                        .status(order.get().getStatus())
                        .amount(order.get().getAmount())
                        .build();  
    } 

    @Transactional(TxType.REQUIRED)
    public boolean delete(int id) {
        Optional<Order> order = orderRepository.findById(id);

        if(order.isEmpty()){
            return false;
        }
        
        orderRepository.delete(order.get());
        return true;
    } 

}
