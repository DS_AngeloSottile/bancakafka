package com.bancaordine.model.service;


import com.bancaordine.model.dto.PaymentDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
public class PaymentListner {
    
    Logger logger = LoggerFactory.getLogger(PaymentListner.class);
    
    private static ObjectMapper mapper = new ObjectMapper();
    
    @Autowired
    private OrderServiceInterface orderService;
    
    @KafkaListener(id = "#{'${spring.application.name}'}", topics = "#{'${ms.topic.payment.input}'}")
    public void listen(String paymentDtoJsonString, Acknowledgment acknowledgment) {
        logger.info("Listener" + paymentDtoJsonString); //una volta arrivato l'ordine bisona deserializzarlo da json ad oggetto e richiamare il service che si vuole
        try {

            PaymentDto paymentDto = mapper.readValue(paymentDtoJsonString, PaymentDto.class);

            //codice della risposra -> ok    KO

            orderService.paymentCheckInfo(paymentDto); //il service lo chiamo dal listner

        } catch (JsonProcessingException e) {
            
            String message = String.format("Send Event Error (%s)", PaymentDto.class.getSimpleName());
            logger.error(message,e);
        }
        acknowledgment.acknowledge();
    } 
}
