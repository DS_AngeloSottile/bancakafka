package com.bancaordine.model.service;

import java.util.List;

import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.dto.OrderDto;
import com.bancaordine.model.dto.OrderDtoRequest;
import com.bancaordine.model.dto.PaymentDto;

public interface OrderServiceInterface {
    //make and order of any type
    OrderDto makeOrder(List<ItemDto> itemsDtoRequest);

    //get an order by passing id
    public OrderDto getOrder(OrderDtoRequest orderDtoRequest);

    //get an order and make update of it
    OrderDto update(List<ItemDto> orderDtoRequest, int id);
            
    //hard delete with items
    boolean delete(int id);

    //check if the order has been payed
    boolean payed(int id);

    //check the status of the payment
    void paymentCheckInfo(PaymentDto paymentDto);
}
