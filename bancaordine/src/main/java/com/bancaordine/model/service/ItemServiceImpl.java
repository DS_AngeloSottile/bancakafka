package com.bancaordine.model.service;

import java.util.Optional;

import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.entityes.Item;
import com.bancaordine.model.repositories.ItemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemServiceImpl implements ItemServiceInterface {

    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private TransfertService transfertService;

    @Override
    public ItemDto store(ItemDto itemDtoRequest) {
        
        Item item = transfertService.dto2EntityItemSingle(itemDtoRequest);
        itemRepository.save(item);

        return itemDtoRequest;
    }

    @Override
    public boolean delete(int id) {
        
        Optional<Item> item = itemRepository.findById(id);

        if(item.isEmpty()){
            return false;
        }
        
        itemRepository.delete( item.get() );

        return true;
    }
    
}
