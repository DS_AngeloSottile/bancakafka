package com.bancaordine.model.service;

import java.util.ArrayList;
import java.util.List;

import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.dto.OrderDto;
import com.bancaordine.model.dto.OrderItemsDto;
import com.bancaordine.model.entityes.Item;
import com.bancaordine.model.entityes.Order;
import com.bancaordine.model.entityes.OrderItems;

import org.springframework.stereotype.Service;

@Service
public class TransfertService {


    public OrderDto entity2DtoOrder(Order order) {

        return  OrderDto.builder()
                .code(order.getCode())
                .amount(order.getAmount())
                .status(order.getStatus())
                .build();        
    }

    public Order dto2EntityOrder(OrderDto orderDto,Order order) {
        

        order.setStatus(orderDto.getStatus());
        order.setAmount(orderDto.getAmount());
        order.setCode(orderDto.getCode());

        return  order;
    }

    

    public List<OrderItems> dto2EntityListOrderItems(List<OrderItemsDto> orderItemsDto){
        List<OrderItems> items = new ArrayList<>();

        for (OrderItemsDto itemDto : orderItemsDto) {

            items.add( OrderItems.builder()
                        .quantity(itemDto.getQuantity())
                        .build() );
            }
        return  items;
    }

    public List<OrderItemsDto> entity2DtoListOrderItems(List<OrderItems> orderItemsList){
        List<OrderItemsDto> itemsDto = new ArrayList<>();

        for (OrderItems orderItems : orderItemsList) {

            itemsDto.add( OrderItemsDto.builder()
                        .quantity(orderItems.getQuantity())
                        .item(entity2DtoItemSingle( orderItems.getItem() ))
                        .build() );
            }
        return  itemsDto;
    }

    public Item dto2EntityItemSingle(ItemDto itemDto){

            return  Item.builder()
                    .amount(itemDto.getAmount())
                    .name(itemDto.getName())
                    .build();
    }

    public ItemDto entity2DtoItemSingle(Item item){

        return  ItemDto.builder()
                    .amount(item.getAmount())
                    .name(item.getName())
                    .build();
    }
}
