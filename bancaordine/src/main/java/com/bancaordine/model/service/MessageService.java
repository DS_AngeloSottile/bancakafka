package com.bancaordine.model.service;

import java.util.concurrent.ExecutionException;

import com.bancaordine.exception.EventSendingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
public class MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);
    private static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
    public void syncSend(Object payload,String topic){

        try {
            //l'oggetto che arriva va inviato come stringa
            String payloadtConvertedToString = mapper.writeValueAsString(payload);

            trySend(payloadtConvertedToString,true,topic);
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", payload.getClass().getSimpleName());
            LOGGER.error(message,e);
            throw new EventSendingException(message,e);
        }
        
    }

    public void asyncSend(Object payload,String topic){

        try {
            //l'oggetto che arriva va inviato come stringa 
            String payloadtConvertedToString = mapper.writeValueAsString(payload);
            trySend(payloadtConvertedToString,false,topic);
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", payload.getClass().getSimpleName());
            LOGGER.error(message,e);
            throw new EventSendingException(message,e);
        }
        
    }

    private void trySend(String payloadtConvertedToString,boolean blocking,String topic) {
        boolean done = false;
        String errorMessage = null;
        int attempts = 10;
        for(int i = 0; i < attempts; i++){ 
            try {     
                //altrimenti ListenableFuture<SendResult<K, V>>​                            //questo sarebbe l'output
                ListenableFuture<SendResult<String, String>> result = kafkaTemplate.send(topic,payloadtConvertedToString);
                if(blocking){
                    result.get();
                }

                done = true;
                //se è stato mandato correttamente il ciclo for viene interotto
                break; 
            } catch (InterruptedException e) {
                LOGGER.error("Message error", e);
                //se si ha un Interrupt exception interompimo il thread("se sto spegnendo l'applicazione")
                Thread.currentThread().interrupt(); 
            } catch ( ExecutionException e) {
                LOGGER.error("Message error", e);
                errorMessage = e.getLocalizedMessage();
                sleep();
            }
        }
        if(!done){
            throw new EventSendingException(errorMessage);
        }
    }

    private static void sleep() {
        try {
            int time = 5000;
            Thread.sleep(time);
        } catch (InterruptedException e1) {
            Thread.currentThread().interrupt();
        }
    } 

    
}
