package com.bancaordine.model.service;

import com.bancaordine.model.dto.ItemDto;

public interface ItemServiceInterface {

    ItemDto store(ItemDto itemDtoRequest);

    boolean delete(int id);
    
}
