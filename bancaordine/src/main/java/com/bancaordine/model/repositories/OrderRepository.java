package com.bancaordine.model.repositories;

import com.bancaordine.model.entityes.Order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order,Integer> {
   
    Order findOrderByCode(String code);
}
