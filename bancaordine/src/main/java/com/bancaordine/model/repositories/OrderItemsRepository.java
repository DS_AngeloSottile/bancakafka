package com.bancaordine.model.repositories;

import com.bancaordine.model.entityes.OrderItems;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemsRepository extends JpaRepository<OrderItems,Integer>{
    
}
