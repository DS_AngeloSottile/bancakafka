package com.bancaordine.model.repositories;

import com.bancaordine.model.entityes.Item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item,Integer> {
    
    Item findItemByName(String name);
}
