package com.bancaordine.model.entityes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "order_items")
public class OrderItems {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
    private int id;
    
    @ManyToOne
	@JoinColumn(name="order_id",referencedColumnName = "id",nullable = true)
	@JsonManagedReference
    private Order order;
    
    @ManyToOne
	@JoinColumn(name="item_id",referencedColumnName = "id",nullable = true)
	@JsonManagedReference
    private Item item;

    @Column(name = "quantity")
    private int quantity;

}
