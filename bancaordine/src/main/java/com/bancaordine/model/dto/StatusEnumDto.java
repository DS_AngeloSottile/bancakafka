package com.bancaordine.model.dto;

public enum StatusEnumDto {
    ACQUISTATO,
    VERIFICANDO,
    NON_ACCETTATO,
    PAGATO
}
