package com.bancaordine.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import com.bancaordine.exception.ItemExistException;
import com.bancaordine.config.JpaConfig;
import com.bancaordine.model.dto.ItemDto;
import com.bancaordine.model.dto.OrderDto;
import com.bancaordine.model.dto.PaymentDto;
import com.bancaordine.model.dto.StatusEnumDto;
import com.bancaordine.model.entityes.Item;
import com.bancaordine.model.entityes.Order;
import com.bancaordine.model.repositories.ItemRepository;
import com.bancaordine.model.repositories.OrderItemsRepository;
import com.bancaordine.model.repositories.OrderRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = { TransfertService.class, OrderItemsRepository.class, OrderServiceImpl.class ,OrderRepository.class, JpaConfig.class}  )
@TestInstance(TestInstance.Lifecycle.PER_CLASS)  
public class OrderServiceTest {
/*
    @Autowired
    OrderServiceInterface orderService;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    TransfertService transfertService;

    @Autowired
    private OrderItemsRepository orderItemsRepository;

    @BeforeEach
    void setUp(){
        orderRepository.deleteAll();
        orderItemsRepository.deleteAll();
        orderRepository.deleteAll();
    }
    
    @Test
    void dto2entityOrder(){
        OrderDto orderDto = OrderDto.builder().amount(30).status(StatusEnumDto.ACQUISTATO.toString()).code("432ghj432hjhgj").build();

        Order order = new Order();

        transfertService.dto2EntityOrder(orderDto, order);

        assertEquals(order.getAmount(), orderDto.getAmount());
        assertEquals(order.getStatus(), orderDto.getStatus());
        assertEquals(order.getCode(), orderDto.getCode());
    }

    @Test
    void entity2dtoOrder(){
        Order order = Order.builder().amount(30).status(StatusEnumDto.ACQUISTATO.toString()).build();

        OrderDto orderDto = transfertService.entity2DtoOrder(order);

        assertEquals(order.getAmount(), orderDto.getAmount());
        assertEquals(order.getStatus(), orderDto.getStatus());
        assertEquals(order.getCode(), orderDto.getCode());
    }

    @Test
    @Transactional
    void getOrder(){

        Order order = Order.builder().id(1).amount(30).status(StatusEnumDto.NON_ACCETTATO.toString()).build();
        orderRepository.save(order);

        OrderDto orderDto = orderService.getOrder(1);
        
        assertEquals(30, orderDto.getAmount() );

    }

    @Test
    void makeOrder(){
        Item item = Item.builder().amount(100).name("giocattoli").build();
        itemRepository.save(item);

        List<ItemDto> itemsDto = new ArrayList<>(Arrays.asList(
           ItemDto.builder().name("giocattoli").quantity(2).build()
        ));


        orderService.makeOrder(itemsDto);

        assertEquals(1,orderRepository.findAll().size() );
        assertEquals(200,orderRepository.findById(1).get().getAmount() );
        assertEquals(1,orderItemsRepository.findAll().size() );
        assertEquals(2,orderItemsRepository.findById(1).get().getQuantity());
    } 

    @Test
    void makeOrderException(){
        Item item = Item.builder().amount(100).name("item1").build();
        itemRepository.save(item);

        List<ItemDto> itemsDto = new ArrayList<>(Arrays.asList(
           ItemDto.builder().name("giocattoli").quantity(2).build()
        ));

        assertThrows(ItemExistException.class,() ->  orderService.makeOrder(itemsDto));
    } 

    @Test
    void payedStatusTrue(){
        Order order = Order.builder().amount(20).code("ewqewqewq").status(StatusEnumDto.PAGATO.toString()).build();
        orderRepository.save(order);

        assertEquals( true, orderService.payed(1) );
    } 
    
    @Test
    void payedStatusFalse(){
        Order order = Order.builder().amount(20).code("ewqewqewq").status(StatusEnumDto.NON_ACCETTATO.toString()).build();
        orderRepository.save(order);

        assertEquals( false, orderService.payed(1) );
    } 
    
    @Test
    void paymentCheckInfo(){
        Order order = Order.builder().amount(20).code("4432rw65egfd").status(StatusEnumDto.NON_ACCETTATO.toString()).build();
        orderRepository.save(order);

        //NON_ACCETATTO
        PaymentDto paymentDto = PaymentDto.builder().orderCode("4432rw65egfd").status("rifiutato").build();
        orderService.paymentCheckInfo(paymentDto);
        assertEquals("NON_ACCETTATO", orderRepository.findOrderByCode("4432rw65egfd").getStatus());

        //VERIFICANDO
        PaymentDto paymentDto2 = PaymentDto.builder().orderCode("4432rw65egfd").status("verifica").build();
        orderService.paymentCheckInfo(paymentDto2);
        assertEquals("VERIFICANDO", orderRepository.findOrderByCode("4432rw65egfd").getStatus());
        
        //PAGATO
        PaymentDto paymentDto3 = PaymentDto.builder().orderCode("4432rw65egfd").status("pagato").build();
        orderService.paymentCheckInfo(paymentDto3);
        assertEquals("PAGATO", orderRepository.findOrderByCode("4432rw65egfd").getStatus());
    } 

    @Test
    void updateOrder(){
        Item item = Item.builder().amount(100).name("giocattoli").build();
        itemRepository.save(item);

        List<ItemDto> itemsDto = new ArrayList<>(Arrays.asList(
           ItemDto.builder().name("giocattoli").quantity(2).build()
        ));
        
        Order order = Order.builder().amount(20).code("4432rw65egfd").status(StatusEnumDto.NON_ACCETTATO.toString()).build();
        orderRepository.save(order);


        OrderDto orderDto = orderService.update(itemsDto,1);

        assertEquals("4432rw65egfd",orderDto.getCode() );
        assertEquals(200,orderDto.getAmount() );
        assertEquals("VERIFICANDO",orderDto.getStatus());
    } 

    @Test
    void deleteOrder(){ 
        
        Order order = Order.builder().amount(20).code("4432rw65egfd").status(StatusEnumDto.NON_ACCETTATO.toString()).build();
        orderRepository.save(order);

        //delete if order is inside the database
        assertEquals(true,  orderService.delete(1));
        
        //return false if order isn't inside the database
        assertEquals(false,  orderService.delete(4));
    } */
}
