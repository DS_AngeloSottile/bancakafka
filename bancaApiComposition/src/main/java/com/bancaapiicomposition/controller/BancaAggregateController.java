package com.bancaapiicomposition.controller;


import com.bancaapiicomposition.model.dto.OrderDtoRequest;
import com.bancaapiicomposition.model.dto.OrderPaymentAggregateDto;
import com.bancaapiicomposition.model.service.OrderPaymentAggregationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class BancaAggregateController { 
    
    @Autowired
    public OrderPaymentAggregationService ordinePaymentAgregation; 

    @PostMapping(value = "/orderPaymentAgregetion")
    public Mono<OrderPaymentAggregateDto>  getOrderPaymentAgregetion(@RequestBody OrderDtoRequest orderDtoRequest){ 
        return ordinePaymentAgregation.getOrderPpaymentAggregation(orderDtoRequest);
    }

}
