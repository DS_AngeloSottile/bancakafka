package com.bancaapiicomposition.controller;

import com.bancaapiicomposition.exception.FutureException;
import com.bancaapiicomposition.model.dto.ErrorDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice(basePackages = "com.bancaapiicomposition.controller")
public class ErrorHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

  @ExceptionHandler(FutureException.class)
  @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
  public ErrorDto handleCustomException(FutureException exception) {
    LOGGER.info("msg");
    return ErrorDto.builder().message(exception.getMessage()).code(exception.getCode()).build();
    
  }
}
