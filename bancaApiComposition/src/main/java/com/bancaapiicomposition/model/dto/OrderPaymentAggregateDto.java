package com.bancaapiicomposition.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderPaymentAggregateDto {
 
    private OrderDto orderDto;
    private PaymentDto paymentDto;
}
