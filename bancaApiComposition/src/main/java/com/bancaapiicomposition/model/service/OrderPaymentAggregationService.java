package com.bancaapiicomposition.model.service;


import com.bancaapiicomposition.model.dto.OrderDto;
import com.bancaapiicomposition.model.dto.OrderDtoRequest;
import com.bancaapiicomposition.model.dto.OrderPaymentAggregateDto;
import com.bancaapiicomposition.model.dto.PaymentDto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@Service
public class OrderPaymentAggregationService {
    
    @Value(value = "${order.host.url}")
    private String baseOrderUrl;

    @Value(value = "${payment.host.url}")
    private String basePaymentUrl;

    public Mono<OrderPaymentAggregateDto> getOrderPpaymentAggregation(OrderDtoRequest orderDtoRequest){
                                                                                //passa gli oggetti nel costruttore
        Mono<OrderPaymentAggregateDto> orderPaymentAggregateDto = Mono.zip( getOrder(orderDtoRequest), getPayment(orderDtoRequest),OrderPaymentAggregateDto::new );
        return orderPaymentAggregateDto;
        // orderPaymentAggregateDto.block().getOrderDto(); if i want the entity
    }

    public Mono<OrderDto> getOrder(OrderDtoRequest orderDtoRequest){
        
        WebClient webClient = WebClient.create(baseOrderUrl);

        return webClient.post()
                        .uri("/api/order/get/")
                        .body(BodyInserters.fromValue(orderDtoRequest))
                        .retrieve()
                        .bodyToMono(OrderDto.class);
    }

    public Mono<PaymentDto> getPayment(OrderDtoRequest orderDtoRequest){  

        WebClient webClient = WebClient.create(basePaymentUrl);

        return webClient.post()
                        .uri("/api/payment/getPayment")
                        .body(BodyInserters.fromValue(orderDtoRequest))
                        .retrieve()
                        .bodyToMono(PaymentDto.class);
    }
    
}
