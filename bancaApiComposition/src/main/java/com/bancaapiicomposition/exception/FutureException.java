package com.bancaapiicomposition.exception;

public class FutureException extends RuntimeException {

    private final Integer code;

    public FutureException(String messageString, Integer code) {
		super(messageString);
		this.code = code;
	}

	public Integer getCode() {
	  return this.code;
	}
    
}
