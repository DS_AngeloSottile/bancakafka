package com.bancaapiicomposition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancaApiCompositionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancaApiCompositionApplication.class, args);
	}

}
