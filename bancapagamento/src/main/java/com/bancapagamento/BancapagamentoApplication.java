package com.bancapagamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@SpringBootApplication
public class BancapagamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancapagamentoApplication.class, args);
	}

	@Bean
	public OpenAPI getOpenAPI() {
		return new OpenAPI()
				.components(new Components())
				.info(new Info().title("Bank API").version("1.0.0")
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));
	}

}
