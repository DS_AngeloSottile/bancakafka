package com.bancapagamento.controller;

import com.bancapagamento.model.dto.ErrorDto;
import com.bancapagamento.model.dto.OrderDto;
import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.dto.PaymenteResponseDto;

import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;

public interface PaymentControllerInterface {
    
    @Operation(summary = "Make a payment and save it to database")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return an object PaymenteResponseDto with a string with the status of the payment", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = PaymenteResponseDto.class)) 
                                    }),
                    }) 
    public ResponseEntity<PaymenteResponseDto> makePayment( OrderDto orderDto);

    @Operation(summary = "Make an update to the payment by changing the status, payed, refused, verifyng")
    @ApiResponses(value = { 
                        @ApiResponse(
                            responseCode = "200", 
                            description = "the method return an object PaymenteResponseDto with a string contained the current status of the payment", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = PaymenteResponseDto.class)) 
                                    }),
                        @ApiResponse(
                            responseCode = "404", 
                            description = "a payment with the given id is not present to the database", 
                            content = { 
                                    @Content(
                                        mediaType = "application/json", 
                                        schema = @Schema(implementation = ErrorDto.class)) 
                                    }),
                    }) 
    public ResponseEntity<PaymenteResponseDto> updateStatusPayment( PaymentDto paymentDto );

}
