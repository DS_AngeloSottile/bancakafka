package com.bancapagamento.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.bancapagamento.model.dto.OrderDto;
import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.dto.PaymenteResponseDto;
import com.bancapagamento.model.service.PaymentServiceInterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("api/payment")
public class PaymentController implements PaymentControllerInterface {
    
    @Autowired
    private PaymentServiceInterface paymentService;


    @PostMapping("/makePayment")                            //questo mi arriva da kafka
    public ResponseEntity<PaymenteResponseDto> makePayment(@RequestBody OrderDto orderDto){
        PaymenteResponseDto paymenteResponseDto = paymentService.makePayment(orderDto);
        //shoul be ok
        return new ResponseEntity<>(paymenteResponseDto,HttpStatus.OK);
    }



    @PostMapping("/updateStatusPayment")                // questo mi arriva da kafka
    public ResponseEntity<PaymenteResponseDto> updateStatusPayment(@RequestBody PaymentDto paymentDto ){
        PaymenteResponseDto paymenteResponseDto = paymentService.updateStatusPayment(paymentDto);

        return new ResponseEntity<>(paymenteResponseDto,HttpStatus.OK);
    }

    @PostMapping(value = "/getPayment")
    public ResponseEntity<PaymentDto> getPayment(@RequestBody OrderDto orderDto){
        System.out.println(orderDto.getCode());
        PaymentDto paymentDto = paymentService.getPayment(orderDto.getCode());

        return new ResponseEntity<>(paymentDto,HttpStatus.OK);
    }

}
