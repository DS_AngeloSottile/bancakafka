package com.bancapagamento.controller;

import com.bancapagamento.exception.NotFoundCustomException;
import com.bancapagamento.model.dto.ErrorDto;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice(basePackages = "com.bancapagamento.controller")
public class ErrorHandler {


  @ExceptionHandler(NotFoundCustomException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorDto handleCustomException(NotFoundCustomException exception) {

    return ErrorDto.builder().message(exception.getMessage()).code(exception.getCode()).build();
    
  }

}