package com.bancapagamento.model.service;

import com.bancapagamento.model.dto.OrderDto;
import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.dto.PaymenteResponseDto;

public interface PaymentServiceInterface {
    
    public PaymenteResponseDto makePayment(OrderDto orderDto);
    
    public PaymenteResponseDto updateStatusPayment(PaymentDto paymentDto);

    public PaymentDto getPayment(String orderCode);

}
