package com.bancapagamento.model.service;

import java.util.Optional;

import com.bancapagamento.exception.NotFoundCustomException;
import com.bancapagamento.model.dto.OrderDto;
import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.dto.PaymenteResponseDto;
import com.bancapagamento.model.dto.StatusPaymentEnumDto;
import com.bancapagamento.model.entities.Payment;
import com.bancapagamento.model.repositories.PaymentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class PaymentsServiceImpl implements PaymentServiceInterface {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private MessageService messageService;

    @Autowired
    private TransferService transferService;

    @Override
    public PaymenteResponseDto makePayment(OrderDto orderDto) {

        if(orderDto.getStatus().equals("pagato")){ //contrario
            return PaymenteResponseDto.builder().message("Ordine già pagato").build();
        }
        
        Payment payment = Payment.builder()
                                    .price(orderDto.getAmount())
                                    .status(StatusPaymentEnumDto.IN_VERIFICA.toString())
                                    .orderCode(orderDto.getCode())
                                    .build();
                                    
        paymentRepository.save(payment);

        return PaymenteResponseDto.builder().message(payment.getStatus()).build();
    }

    @Override
    public PaymenteResponseDto updateStatusPayment(PaymentDto paymentDto) {

        Optional<Payment> payment = paymentRepository.findById(paymentDto.getId());

        if(payment.isEmpty()){
            throw new NotFoundCustomException("payment not founded",HttpStatus.NOT_FOUND.value());
        }

        switch (paymentDto.getStatus().toLowerCase()) {
            case "pagato":
                payment.get().setStatus(StatusPaymentEnumDto.PAGATO.toString());
                break;
        
            case "verifica":
                payment.get().setStatus(StatusPaymentEnumDto.IN_VERIFICA.toString());
                break;
            case "rifiutato":
                payment.get().setStatus(StatusPaymentEnumDto.RIFIUTATO.toString());
                break;

            default:
                break;
        }
        paymentRepository.save(payment.get());

        PaymentDto paymentDtoToSend = PaymentDto.builder()
                                            .status(payment.get().getStatus())
                                            .id(payment.get().getId())
                                            .orderCode(payment.get().getOrderCode())
                                            .build();
        messageService.syncSend(paymentDtoToSend, "paymentCheckInfo");

        return PaymenteResponseDto.builder().message(payment.get().getStatus()).build();
    }

    @Override
    public PaymentDto getPayment(String orderCode) {

        Payment payment = paymentRepository.findByOrderCode(orderCode);

        return  transferService.entity2DtoPayment(payment);
    }
    
}
