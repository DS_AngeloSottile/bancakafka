package com.bancapagamento.model.service;

import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.entities.Payment;

public interface TransferServiceInterface {
    
    public Payment dto2EntityPayment(PaymentDto paymentDto);
}
