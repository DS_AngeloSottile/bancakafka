package com.bancapagamento.model.service;


import com.bancapagamento.model.dto.OrderDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
public class OrderListner {
    
    Logger logger = LoggerFactory.getLogger(OrderListner.class);

    private static ObjectMapper mapper = new ObjectMapper();
    
    @Autowired
    private PaymentServiceInterface paymentService;
                                                                     //questo sarebbe l'input
    @KafkaListener(id = "#{'${spring.application.name}'}", topics = "#{'${ms.topic.order.input}'}")
    public void listen(String orderDtoJsonString, Acknowledgment acknowledgment) {
        logger.info(orderDtoJsonString); //una volta arrivato l'ordine bisona deserializzarlo da json ad oggetto e richiamare il service che si vuole
        try {

            OrderDto orderDto = mapper.readValue(orderDtoJsonString, OrderDto.class);
            paymentService.makePayment(orderDto); //il service lo chiamo dal listner

        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", OrderDto.class.getSimpleName());
            logger.error(message,e);
        }
        acknowledgment.acknowledge();
    } 
}
