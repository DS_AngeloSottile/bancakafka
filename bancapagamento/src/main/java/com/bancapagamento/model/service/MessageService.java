package com.bancapagamento.model.service;

import java.util.concurrent.ExecutionException;

import com.bancapagamento.exception.EventSendingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
public class MessageService {

    Logger logger = LoggerFactory.getLogger(MessageService.class);
    private static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;
    
    public void syncSend(Object payload,String topic){

        try {
            String payloadtConvertedToString = mapper.writeValueAsString(payload);//l'oggetto che arriva va inviato come stringa

            trySend(payloadtConvertedToString,true,topic);
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", payload.getClass().getSimpleName());
            logger.error(message,e);
            throw new EventSendingException(message,e);
        }
        
    }

    public void asyncSend(Object payload,String topic){

        try {
            String payloadtConvertedToString = mapper.writeValueAsString(payload);//l'oggetto che arriva va inviato come stringa 
            trySend(payloadtConvertedToString,false,topic);
        } catch (JsonProcessingException e) {
            String message = String.format("Send Event Error (%s)", payload.getClass().getSimpleName());
            logger.error(message,e);
            throw new EventSendingException(message,e);
        }
        
    }

    private void trySend(String payloadtConvertedToString,boolean blocking,String topic) {
        boolean done = false;
        String errorMessage = null;
        for(int i = 0; i < 10; i++){ //un excecution Exception deve 
            try {                                                                        //questo sarebbe l'output
                ListenableFuture<SendResult<String, String>> result = kafkaTemplate.send(topic,payloadtConvertedToString);  //altrimenti ListenableFuture<SendResult<K, V>>​
                if(blocking){
                    result.get();
                }

                done = true;
                break; //se è stato mandato correttamente il ciclo for viene interotto
            } catch (InterruptedException e) {
                logger.error("Message error", e);
                Thread.currentThread().interrupt(); //se si ha un Interrupt exception interompimo il thread("se sto spegnendo l'applicazione")
            } catch ( ExecutionException e) {
                logger.error("Message error", e);
                errorMessage = e.getLocalizedMessage();
                sleep();
            }
        }
        if(!done){
            throw new EventSendingException(errorMessage);
        }
    }

    private void sleep() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e1) {
            Thread.currentThread().interrupt();
        }
    } 

    
}
