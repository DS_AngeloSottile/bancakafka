package com.bancapagamento.model.service;

import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.entities.Payment;

import org.springframework.stereotype.Service;

@Service
public class TransferService {
    
    public Payment dto2EntityPayment(PaymentDto paymentDto){
        return Payment.builder()
                        .status(paymentDto.getStatus())
                        .build();
    }

    public PaymentDto entity2DtoPayment(Payment payment){
        return PaymentDto.builder()
                        .status(payment.getStatus())
                        .orderCode(payment.getOrderCode())
                        .build();
    }
}
