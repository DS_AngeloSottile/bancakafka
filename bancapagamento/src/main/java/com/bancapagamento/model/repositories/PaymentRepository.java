package com.bancapagamento.model.repositories;

import com.bancapagamento.model.entities.Payment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment,Integer> {
    
    public Payment findByOrderCode(String orderCode);
}
