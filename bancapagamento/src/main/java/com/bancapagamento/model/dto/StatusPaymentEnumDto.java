package com.bancapagamento.model.dto;

public enum StatusPaymentEnumDto {
    IN_VERIFICA,
    RIFIUTATO,
    PAGATO
}
