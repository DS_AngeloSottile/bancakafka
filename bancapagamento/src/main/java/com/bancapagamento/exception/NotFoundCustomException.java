package com.bancapagamento.exception;

public class NotFoundCustomException extends RuntimeException {
    private Integer code;

    public NotFoundCustomException(String messageString, Integer code) {
		super(messageString);
		this.code = code;
	}

	public Integer getCode() {
	  return this.code;
	}
  
	public void setCode(Integer code) {
	  this.code = code;
	}
}
