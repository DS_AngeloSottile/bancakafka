package com.bancapagamento.model.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import com.bancapagamento.config.JpaConfig;
import com.bancapagamento.exception.NotFoundCustomException;
import com.bancapagamento.model.dto.OrderDto;
import com.bancapagamento.model.dto.PaymentDto;
import com.bancapagamento.model.dto.PaymenteResponseDto;
import com.bancapagamento.model.entities.Payment;
import com.bancapagamento.model.repositories.PaymentRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = { TransferService.class, PaymentsServiceImpl.class,PaymentRepository.class ,JpaConfig.class}  )
@TestInstance(TestInstance.Lifecycle.PER_CLASS)  
public class PaymentServiceTest {
/*
    @Autowired
    TransferService transferService;

    @Autowired
    PaymentServiceInterface paymentService;

    @Autowired
    PaymentRepository paymentRepository;

    @BeforeEach
    void deleteAll(){
        paymentRepository.deleteAll();
    }

    @Test
    void dto2EntityPayment(){
        PaymentDto paymentDto = PaymentDto.builder().status("pagato").id(1).build();

        Payment payment = transferService.dto2EntityPayment(paymentDto);

        assertEquals(payment.getStatus(),paymentDto.getStatus());
    }

    @Test
    void makePayment(){
        OrderDto orderDto = OrderDto.builder().amount(200).code("b6ee2ab0-2f0d-4203-ae81-ff6365490c13").status("verifica").build();

        PaymenteResponseDto paymenteResponseDto = paymentService.makePayment(orderDto);

        assertEquals(1, paymentRepository.findAll().size());
        assertEquals("IN_VERIFICA", paymenteResponseDto.getMessage());

    }
    
    @Test
    void updateStatusPayment(){
        Payment payment = Payment.builder().price(200).orderCode("b6ee2ab0-2f0d-4203-ae81-ff6365490c13").status("pagato").id(1).build();
        paymentRepository.save(payment);


        PaymentDto paymentDto = PaymentDto.builder().id(1).status("verifica").build();
        paymentService.updateStatusPayment(paymentDto);

        Optional<Payment> payment2 = paymentRepository.findById(paymentDto.getId());

        if(payment2.isEmpty()){
            throw new NotFoundCustomException("payment not founded",HttpStatus.NOT_FOUND.value());
        }

        assertEquals("IN_VERIFICA",payment2.get().getStatus());

    } */
}
